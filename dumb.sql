DROP TABLE IF EXISTS operations;
CREATE TABLE operations (  
    id int NOT NULL primary key AUTO_INCREMENT COMMENT  'primary key',
    date DATE COMMENT 'Date',
    operation VARCHAR (255) COMMENT  'Operation description',
    category VARCHAR (255) COMMENT 'Operation category',
    total DOUBLE COMMENT 'Amount of money spent'
) default charset utf8;

INSERT INTO operations (date, operation, category, total)
VALUES ('2021-08-23', 'Lunch in restoran', 'Day outs', 23.40),
('2021-09-06', 'Disco', 'Night outs', 23.40),
('2021-11-12', 'Birthday', 'Party', 23.40),
('2021-11-27', 'Bar', 'Night outs', 23.40),
('2021-12-13', 'Apartement rent', 'Rent', 23.40);