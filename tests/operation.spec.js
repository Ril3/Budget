import { server } from '../src/server';
import request from 'supertest';


describe('Operations root', ()=>{
    it('should return all operations on GET', async()=>{
        let response = await request(server)
            .get('/api/operations/find/all')
            .expect(200)
            expect(response.body).toContainEqual({
                id: expect.any(Number),
                date: expect.any(String),
                operation: expect.any(String),
                category: expect.any(String),
                total: expect.any(Number)
            })
    })

    it('should return all operation in one category on GET', async()=>{
        let response = await request(server)
            .get('/api/operations/category/night outs')
            .expect(200)
            expect(response.body).toContainEqual({
                id: expect.any(Number),
                date: expect.any(String),
                operation: expect.any(String),
                category: expect.any(String),
                total: expect.any(Number)
            })
    })


    it('should return 404 on GET with unexisting id', async () => {
        await request(server)
            .get('/api/operation/find/9999')
            .expect(404);
    });
})
