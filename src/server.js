import express from 'express';
import { operationController } from './controller/OperationControler';

export const server = express();

server.use(express.json())

server.use('/api/operations', operationController)