export class Operation {
    id;
    date;
    operation;
    category;
    total;
    /**
     * 
     * @param {String} date 
     * @param {String} operation 
     * @param {String} category 
     * @param {Number} total 
     * @param {Number} id 
     */
    constructor(date, operation, category, total, id=null){
        this.date = date;
        this.operation = operation;
        this.category  = category;
        this.total = total;
        this.id = id;
    }
}