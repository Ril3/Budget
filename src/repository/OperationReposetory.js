import { Operation } from '../entity/Operation';
import { connection } from './connection';


export class OperationReposetory {

    /**
     * Methode that find all operations
     * @returns {Promise<Operation>} table of operations
     */
    static async findAll() {
                const [rows] = await connection.execute('SELECT * FROM operations');
                const operations = [];
                for (const row of rows) {
                    let instance = new Operation(row.date, row.operation, row.category, row.total,row.id);
                    operations.push(instance);
                }
                return operations;
            }

    /**
     * Methode that find one operation by id
     * @param {Number} id of one operation
     * @returns {Promise<Operation>} object of operation
     */
    static async findById(id){
        const [rows] = await connection.execute('SELECT * FROM operations WHERE id=?', [id]);
        if(rows.length === 1){
            return new Operation(rows[0].date, rows[0].operation, rows[0].category, rows[0].total, rows[0].id)
        }
        return null;
    }
       
    /**
     * Methode that accept an object as parametre and use it to change information in certain row
     * @param {Object} paraOper of one operation
     */
    static async edit(paraOper){
            const [rows] = await connection.execute('UPDATE operations SET date=?, operation=?, category=?, total=? WHERE id=?', [paraOper.date, paraOper.operation, paraOper.category, paraOper.total, paraOper.id])
         }

    /**
     * Methode that accept id as parametre and use it to find and delete in same time one operation
     * @param {Number} id of an operation
     */
    static async delete(id){
            const [rows] = await connection.execute('DELETE FROM operations WHERE id=?', [id]);
        }
    /**
     *Methode that accept object as an parametre and use it to create new row in db with cooresponding info. 
     * @param {Object} oper 
     */
    static async add(oper) {
            await connection.execute('INSERT INTO operations (date, operation, category, total) VALUES (?,?,?,?)', [oper.date, oper.operation, oper.category, oper.total]);
        }

    /**
     * Methode that accept 3 parametres and find all operations that coorespond demanded category...
     * @param {String} category 
     * @param {Number} LIMIT 
     * @param {Number} OFFSET 
     * @returns 
     */
    static async findByCategory(category, LIMIT, OFFSET) {
            const [rows] = await connection.execute('SELECT * FROM operations WHERE category=? LIMIT ? OFFSET ?', [category, LIMIT, OFFSET]);
            const operations = [];
            for (const row of rows) {
                let instance = new Operation(row.date, row.operation, row.category, row.total, row.id);
                operations.push(instance);
        }
        return operations;
    }
    
    /**
     * Methode that takes date as one parametre and use it to display operations with cooresponding month
     * @param {Date} date 
     * @returns {Promise<Operation>}
     */
    static async findByDate(date){
        const [rows] = await connection.execute('SELECT * FROM operations WHERE MONTH(date)=?', [date]);
    
        const operations = [];
        for(const row of rows){
            let instance = new Operation(row.date, row.operation, row.category, row.total, row.id);
            operations.push(instance);
        }
        return operations;
    }
}
