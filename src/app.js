import 'dotenv-flow/config';
import { server } from "./server";

const port = process.env.PORT || 3000;

server.listen(3000, ()=>{
    console.log('listening  http://localhost:'+port)
})


process.on('SIGINT', () => { 
    console.log("exiting…"); 
    process.exit(0); 
});

process.on('exit', () => { 
    console.log("exiting…"); 
    process.exit(0); 
});
