import {Router} from "express";
import { OperationReposetory } from "../repository/OperationReposetory";

export const operationController = Router();

//Find all operations
operationController.get('/find/all',async (req,res)=>{
    let operations = await OperationReposetory.findAll();
    res.json(operations)
});

//Find one operation by ID
operationController.get('/find/:id', async (req, res)=>{
    let operation = await OperationReposetory.findById(req.params.id);
    if(!operation){
        resp.status(404).json ({error : 'Operation is not found!!!'});
        return;
    }
    res.json(operation);
}) 

//Edit one operation
operationController.put('/edit', async (req, res)=>{
    await OperationReposetory.edit(req.body);
    res.end();
})

//Delete one operation by ID
operationController.delete('/delete/:id',async (req,resp)=>{
    try{
        await OperationReposetory.delete(req.params.id);
        resp.status(204).end()
        console.log(`Operation ${req.params.id} is deleted!!!`)
    }catch (e){
        console.log(e)
        resp.status(500).end()
    }
})

//ADD one new operation
operationController.post('/add', async (req, res)=>{
    await OperationReposetory.add(req.body);
    res.end();
})

//Find operations by category
operationController.get('/category/:category', async (req, resp) => {
        let operation = await OperationReposetory.findByCategory(req.params.category, 4, 0);
        if(!operation) {
            resp.status(404).json({error: 'Not Found'});
            return;
        }
        resp.json(operation);
})

//Find by date
operationController.get('/date/:date', async (req, res)=>{
    let operation = await OperationReposetory.findByDate(req.params.date);
    res.json(operation)
})